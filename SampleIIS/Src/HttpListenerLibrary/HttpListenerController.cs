﻿using System;
using System.Web.Hosting;
using System.Threading;
using System.Diagnostics;

namespace HttpListenerLibrary
{
    public class HttpListenerController
    {
        // Fields
        private Thread _pump;

        private bool _isListening;

        private readonly HttpListenerWrapper _listener;

        public bool IsAlive => _pump != null && _pump.IsAlive;

        public HttpListenerController(string virtualDir, string physicalDir)
        {
            _listener = (HttpListenerWrapper)ApplicationHost.CreateApplicationHost(
                typeof(HttpListenerWrapper),
                virtualDir,
                physicalDir
            );
            _listener.Configure(virtualDir, physicalDir);
        }

        public void Start()
        {
            _isListening = true;
            _pump = new Thread(Pump);
            _pump.Start();
        }

        public void Stop()
        {
            _isListening = false;
            _listener.Stop();
        }

        public void AddPrefix(string prefix)
        {
            _listener.AddPrefix(prefix);
        }

        public void RemovePrefix(string prefix)
        {
            _listener.RemovePrefix(prefix);
        }

        private void Pump()
        {
            _listener.Start();

            while (_isListening)
            {
                try
                {
                    _listener.ProcessRequest();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    Debug.WriteLine(e.ToString());
                }
            }
        }
    }
}