﻿using System;
using System.Diagnostics;

namespace HttpListenerLibrary;

/// <summary>
/// 请求解析层
/// </summary>
internal abstract class RequestParser : BaseHttpWorkerRequest
{
    public override string GetHttpVerbName() => _context.Request.HttpMethod;

    public override string GetHttpVersion() =>
        $"HTTP/{_context.Request.ProtocolVersion.Major}.{_context.Request.ProtocolVersion.Minor}";

    public override string GetLocalAddress()
    {
        var localAddress = _context.Request.LocalEndPoint?.Address.ToString() ?? string.Empty;
        Console.WriteLine($@"GetLocalAddress(): {localAddress}");
        PrintCallerInfo(new StackTrace());
        return localAddress;
    }

    public override int GetLocalPort()
    {
        return _context.Request.LocalEndPoint?.Port ?? 0;
    }

    public override string GetRawUrl()
    {
        // // 获取当前调用堆栈的追踪信息
        // var stackTrace = new StackTrace();
        //
        // // 获取调用者的堆栈帧（索引1表示调用者）
        // var callerFrame = stackTrace.GetFrame(1);
        //
        // // 从堆栈帧中提取方法名称
        // var methodName = callerFrame.GetMethod().Name;
        //
        // // 从堆栈帧中提取文件名
        // var fileName = callerFrame.GetFileName();
        //
        //
        // // 从堆栈帧中提取行号
        // var lineNumber = callerFrame.GetFileLineNumber();
        //
        // // 输出调用者信息
        // Console.WriteLine(callerFrame.ToString());
        // Console.WriteLine($@"Called by: {methodName} at {fileName}:{lineNumber}");
        Console.WriteLine($@"GetRawUrl(): {_context.Request.RawUrl}");
        PrintCallerInfo(new StackTrace());
        return _context.Request.RawUrl;
    }


    public override string GetQueryString()
    {
        var queryString = "";
        var rawUrl = _context.Request.RawUrl;
        // var rawUrl = GetRawUrl();
        var index = rawUrl.IndexOf('?');
        if (index != -1)
        {
            queryString = rawUrl.Substring(index + 1);
        }

        return queryString;
    }

    // ------------------------------------
    // 
    // ------------------------------------

    public override string GetRemoteAddress()
    {
        return _context.Request.RemoteEndPoint?.Address.ToString() ?? string.Empty;
    }

    public override int GetRemotePort()
    {
        return _context.Request.RemoteEndPoint?.Port ?? 0;
    }

    // ------------------------------------
    // 
    // ------------------------------------

    // 运行时用来通知 HttpWorkerRequest 当前的请求已完成的请求处理。
    public override void EndOfRequest()
    {
        //_context.Response.OutputStream.Close();
        _context.Response.Close();
        //_context.Close();
    }
}