﻿#region Using directives

using System;
using System.Net;
using System.Web;
using System.Runtime.Remoting.Lifetime;

#endregion

namespace HttpListenerLibrary
{
    internal class HttpListenerWrapper : MarshalByRefObject
    {
        #region 重构 MarshalByRefObject 成员

        /// <summary>
        /// 用来防止对象过期
        /// </summary>
        /// <returns> </returns>
        public override object InitializeLifetimeService()
        {
            var lease = (ILease)base.InitializeLifetimeService();
            if (lease?.CurrentState == LeaseState.Initial)
            {
                lease.InitialLeaseTime = TimeSpan.Zero;
            }

            return lease;
        }

        #endregion

        private HttpListener _listener;

        private string _virtualDir;

        private string _physicalDir;

        public void Configure(string virtualDir, string physicalDir)
        {
            _virtualDir = virtualDir;
            _physicalDir = physicalDir;
            _listener = new HttpListener();
        }

        public void AddPrefix(string prefix)
        {
            _listener.Prefixes.Add(prefix);
        }

        public void RemovePrefix(string prefix)
        {
            _listener.Prefixes.Remove(prefix);
        }

        public void Start()
        {
            _listener.Start();
        }

        public void Stop()
        {
            //_listener.Abort();
            _listener.Stop();
        }

        public void ProcessRequest()
        {
            HttpListenerContext ctx = null;

            try
            {
                ctx = _listener.GetContext();
            }
            catch
            {
                try
                {
                    ctx?.Response.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                ctx = null;
            }

            if (ctx == null) return;
            var workerRequest = new HttpListenerWorkerRequest(ctx, _virtualDir, _physicalDir);
            if (workerRequest.GetFilePath() == "/")
            {
                workerRequest.SendRedirect("/index.htm");
            }
            else
            {
                HttpRuntime.ProcessRequest(workerRequest);
            }
        }
    }
}