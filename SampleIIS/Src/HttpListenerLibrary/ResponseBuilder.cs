﻿using System;
using System.Diagnostics;
using System.IO;

namespace HttpListenerLibrary
{
    internal abstract class ResponseBuilder : RequestParser
    {
        // ---------------------------------------------------------------------
        // 响应构建层 (ResponseBuilder): 这一层负责构建 HTTP 响应，包括设置响应头、状态码、响应体等。
        // ---------------------------------------------------------------------

        public override void SendStatus(int statusCode, string statusDescription)
        {
            // Console.WriteLine(@"SendStatus {0} {1}", statusCode, statusDescription);
            Console.WriteLine(@"SendStatus(): {0} {1}", statusCode, statusDescription);
            _context.Response.StatusCode = statusCode;
            _context.Response.StatusDescription = statusDescription;
        }

        // 将指定的文件的内容添加到响应，并指定文件和要发送的字节数中的起始位置。
        public override void SendResponseFromFile(string filename, long offset, long length)
        {
            // using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            // {
            //     using (var reader = new BinaryReader(stream))
            //     {
            //         var bytes = reader.ReadBytes((int)length);
            //         _context.Response.ContentLength64 = bytes.Length;
            //         _context.Response.OutputStream.Write(bytes, 0, bytes.Length);
            //         _context.Response.OutputStream.Close();
            //     }
            // }
            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                var bytes = new byte[(int)stream.Length];
                _ = stream.Read(bytes, 0, bytes.Length);
                _context.Response.ContentLength64 = bytes.Length;
                _context.Response.OutputStream.Write(bytes, 0, bytes.Length);
                _context.Response.OutputStream.Close();
            }
        }

        /// <summary>发送所有待发送的响应数据到客户端。</summary>
        /// <param name="finalFlush">
        /// <see langword="true" />如果这是最后一次，待发送的响应数据将被强制写入；否则为<see langword="false" />。
        /// </param>
        public override void FlushResponse(bool finalFlush)
        {
             Console.WriteLine(@"FlushResponse(): {0}", finalFlush);
             PrintCallerInfo(new StackTrace());
            _context.Response.OutputStream.Flush();
        }

        // 将字节数组中的指定的数目的字节添加到响应中。
        public override void SendResponseFromMemory(byte[] data, int length)
        {
            Console.WriteLine(@"SendResponseFromMemory(): {0}", data.Length);
            PrintCallerInfo(new StackTrace());
            _context.Response.OutputStream.Write(data, 0, length);
        }

        // -------------------------------------------------
        // 
        // -------------------------------------------------

        public override void SendKnownResponseHeader(int index, string value)
        {
            _context.Response.Headers[GetKnownResponseHeaderName(index)] = value;
        }

        public override void SendUnknownResponseHeader(string name, string value)
        {
            _context.Response.Headers[name] = value;
        }
    }
}