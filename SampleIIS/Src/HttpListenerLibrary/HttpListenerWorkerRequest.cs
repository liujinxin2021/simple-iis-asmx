﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;

namespace HttpListenerLibrary
{
    internal class HttpListenerWorkerRequest : ResponseBuilder
    {
        /*
         *
         */
        private readonly string _virtualDir;

        private readonly string _physicalDir;

        public HttpListenerWorkerRequest(
            HttpListenerContext context, string virtualDir, string physicalDir)
        {
            if (virtualDir is null or "")
                throw new ArgumentException(nameof(virtualDir));
            if (physicalDir is null or "")
                throw new ArgumentException(nameof(physicalDir));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _virtualDir = virtualDir;
            _physicalDir = physicalDir;
        }


        // 请求的 uri 返回的虚拟路径。
        public override string GetUriPath()
        {
            Console.WriteLine(@"GetUriPath(): {0}", _context.Request.Url.LocalPath);
            PrintCallerInfo(new StackTrace());
            return _context.Request.Url.LocalPath;
        }


        // 将指定的文件的内容添加到响应，并指定文件和要发送的字节数中的起始位置。
        public override void SendResponseFromFile(
            IntPtr handle, long offset, long length)
        {
        }


        // additional overrides
        // 关闭连接
        public override void CloseConnection()
        {
            //_context.Close();
        }

        public override string GetAppPath()
        {
            return _virtualDir;
        }

        // 返回当前正在执行服务器应用程序的物理路径
        public override string GetAppPathTranslated()
        {
            return _physicalDir;
        }

        public override int ReadEntityBody(byte[] buffer, int size)
        {
            return _context.Request.InputStream.Read(buffer, 0, size);
        }

        public override string GetUnknownRequestHeader(string name)
        {
            return _context.Request.Headers[name];
        }

        // 获取所有非标准的 HTTP 标头名称 / 值对
        public override string[][] GetUnknownRequestHeaders()
        {
            // System.Collections.Specialized.NameValueCollection
            var headers = _context.Request.Headers;
            var count = headers.Count;
            var headerPairs = new List<string[]>(count);
            for (var i = 0; i < count; i++)
            {
                var headerName = headers.GetKey(i);
                if (GetKnownRequestHeaderIndex(headerName) != -1) continue;
                var headerValue = headers.Get(i);
                // headerPairs.Add(new[] { headerName, headerValue }); // Use collection expression
                /*
                 * Collection expression 是 C# 12 版本引入的新特性。这种语法允许你以更简洁、更一致的方式初始化集合。
                 * 在 C# 12 中，你可以使用类似于数组或列表字面量的语法来创建集合，而不需要显式调用构造函数或使用 Add 方法。
                 * https://github.com/dotnet/csharplang/blob/main/proposals/csharp-12.0/collection-expressions.md
                 *
                 *
                 * The official repo for the design of the C# programming language
                 * C# 编程语言设计的官方仓库
                 * 
                 */
                headerPairs.Add([headerName, headerValue]);
            }

            var unknownRequestHeaders = headerPairs.ToArray();
            return unknownRequestHeaders;
        }

        public override string GetKnownRequestHeader(int index) => index switch
        {
            HeaderUserAgent => _context.Request.UserAgent ?? "",
            _ => _context.Request.Headers[GetKnownRequestHeaderName(index)]
        };

        // TODO: vet this list
        public override string? GetServerVariable(string name)
        {
            
            switch (name)
            {
                case "HTTPS":
                    return _context.Request.IsSecureConnection ? "on" : "off";
                case "HTTP_USER_AGENT":
                    return _context.Request.Headers["UserAgent"];
                default:
                    return null;
            }
        }

        // 当在派生类中重写请求的 uri 返回的虚拟路径。
        public override string GetFilePath()
        {
            // TODO: this is a hack
            Console.WriteLine(@"GetFilePath(): {0}", GetFilePath(false));
            PrintCallerInfo(new StackTrace());
            return GetFilePath(true);
        }

        // ReSharper disable once UnusedParameter.Local
        private string GetFilePath(bool isInvokeOverride)
        {
            var filePath = _context.Request.Url.LocalPath;
            if (filePath.IndexOf(".aspx", StringComparison.Ordinal) != -1)
                filePath = filePath.Substring(0, filePath.IndexOf(".aspx", StringComparison.Ordinal) + 5);
            else if (filePath.IndexOf(".asmx", StringComparison.Ordinal) != -1)
                filePath = filePath.Substring(0, filePath.IndexOf(".asmx", StringComparison.Ordinal) + 5);
            return filePath;
        }

        public override string GetFilePathTranslated()
        {
            // string s = GetFilePath();
            // s = s.Substring(_virtualDir.Length);
            // s = s.Replace('/', '\\');
            // return _physicalDir + s;
            var s = GetFilePath(false);
            if (s == null) return "";
            s = s.Substring(_virtualDir.Length);
            s = s.Replace('/', '\\');
            if (!s.StartsWith("\\"))
            {
                s = "\\" + s;
            }

            var filePath = _physicalDir + s;
            Console.WriteLine($@"{nameof(GetFilePathTranslated)}(): {filePath}");
            PrintCallerInfo(new StackTrace());
            return filePath;
        }

        // 返回具有 URL 扩展名的资源的附加路径信息。 也就是说，对于路径 /hello/page.htm/tail， GetPathInfo 值是 /tail。
        public override string GetPathInfo()
        {
            var s1 = GetFilePath(false);
            var s2 = _context.Request.Url.LocalPath;
            return s1.Length == s2.Length ? "" : s2.Substring(s1.Length);
        }
    }
}