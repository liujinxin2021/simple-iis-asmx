﻿using System;
using System.Diagnostics;
using System.Net;
using System.Web;

namespace HttpListenerLibrary
{
    internal abstract class BaseHttpWorkerRequest : HttpWorkerRequest
    {
        /*
         * 
         */
        protected HttpListenerContext _context;
        
        /// <summary>
        /// 发送重定向响应
        /// </summary>
        /// <param name="redirectUrl"></param>
        public void SendRedirect(string redirectUrl)
        {
            SendStatus((int)HttpStatusCode.MovedPermanently, "Moved Permanently");
            // SendKnownResponseHeader((int)KnownResponseHeaders.Location, redirectUrl);
            SendKnownResponseHeader(HeaderLocation, redirectUrl);
            EndOfRequest();
        }
        
        protected static void PrintCallerInfo(StackTrace stackTrace)
        {
            // // 获取当前调用堆栈的追踪信息
            // var stackTrace = new StackTrace();
            // 获取调用者的堆栈帧（索引1表示调用者）
            var callerFrame = stackTrace.GetFrame(1);
            // 从堆栈帧中提取方法名称
            var methodName = callerFrame.GetMethod().Name;
            // 从堆栈帧中提取文件名
            var fileName = callerFrame.GetFileName();
            // 从堆栈帧中提取行号
            var lineNumber = callerFrame.GetFileLineNumber();
            // 输出调用者信息
            // Console.WriteLine(callerFrame.ToString());
            Console.WriteLine($@"---- Called by: {methodName} at {fileName}:{lineNumber}");
        }
    }
}