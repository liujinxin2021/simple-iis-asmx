﻿using System.Web.Services;
using HisServiceLibrary.Model;

namespace HisServiceLibrary;

public class HisService
{
    [WebMethod]
    public LoginResult Login(string username, string password)
    {
        if (username == "admin" && password == "111")
        {
            return new LoginResult { IsSuccess = true, Message = "Login Successful" };
        }

        return new LoginResult { IsSuccess = false, Message = "Invalid Username or Password" };
    }
}