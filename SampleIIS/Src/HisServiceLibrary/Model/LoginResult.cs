﻿namespace HisServiceLibrary.Model;

public class LoginResult
{
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
}