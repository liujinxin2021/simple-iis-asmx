﻿using System;

// ReSharper disable UnusedType.Global

namespace HisServiceLibrary.Test;

public class HisServiceTest
{
    public static void Test()
    {
        var service = new HisService();
        var loginResult = service.Login("admin", "111");
        if (loginResult.IsSuccess)
        {
            Console.WriteLine("登录成功");
            Console.WriteLine(loginResult.Message);
        }
        else
        {
            Console.WriteLine("登录失败");
            Console.WriteLine(loginResult.Message);
        }
    }
}