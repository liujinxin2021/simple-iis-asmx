﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using HttpListenerLibrary;

namespace AsmxServer.Wpf;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow
{
    public ObservableCollection<string> PrefixList { get; set; }

    private readonly string _virtualDir;

    private readonly string _physicalDir;

    private HttpListenerController? _controller;

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this; // 使XAML能够访问MainWindow的属性

        PrefixList = new ObservableCollection<string>(new List<string>
            {
                "http://localhost:8081/",
                "http://127.0.0.1:8081/",
            }
        );
        // 获取本机所有 IP 地址
        var addresses = Dns.GetHostAddresses("");
        foreach (var address in addresses)
        {
            if (address.AddressFamily == AddressFamily.InterNetwork)
            {
                Console.WriteLine(address);
                // lbPrefix.Items.Add($"http://{address}:8081/");
                PrefixList.Add($"http://{address}:8081/");
            }
        }

        _virtualDir = "/";
        _physicalDir = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ".."));
    }


    private void bAddPrefix_Click(object sender, RoutedEventArgs e)
    {
        PrefixList.Add(tbPrefix.Text);
    }

    private void ButtonRemovePrefix_Click(object sender, RoutedEventArgs e)
    {
        if (lbPrefix.SelectedIndex < 0)
        {
            return;
        }

        PrefixList.RemoveAt(lbPrefix.SelectedIndex);
    }

    private void bStart_Click(object sender, RoutedEventArgs e)
    {
        _controller = new HttpListenerController(_virtualDir, _physicalDir);
        foreach (var s in PrefixList)
        {
            _controller.AddPrefix(s);
        }

        _controller.Start();
        ButtonStart.IsEnabled = false;
        ButtonStop.IsEnabled = true;
    }

    private void bStop_Click(object sender, RoutedEventArgs e)
    {
        if (_controller == null) return;
        _controller.Stop();
        ButtonStart.IsEnabled = true;
        ButtonStop.IsEnabled = false;
    }

    private void LbPrefix_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        tbPrefix.Text = lbPrefix.SelectedItem?.ToString() ?? string.Empty;
    }

    private void MainWindow_OnClosing(object sender, CancelEventArgs e)
    {
        // 退出提示
        // var result = MessageBox.Show("Are you sure you want to exit?", "Exit", MessageBoxButton.YesNo,
        //     MessageBoxImage.Question);
        // if (result == MessageBoxResult.No)
        // {
        //     e.Cancel = true;
        // }
        if (_controller?.IsAlive == true)
        {
            e.Cancel = true;
            // MessageBox.Show("必须先停止服务器才能退出程序。", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            MessageBox.Show("必须先停止服务器才能退出程序。", this.Title, MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
    }
}