﻿## 访问

http://localhost:8081/index.html
http://192.168.110.101:8081/His.asmx
http://172.22.48.26:8081/index.html

## 计划

### 2024-08-01

计划将WinForm切换为WPF。

## 日志

- 2024-08-01 解决路径错误

### 2024-08-01 解决路径错误

表单的action路径不对

```
<form target="_blank" action="http://[0000:0000:0000:0000:0000:0000:0000:0001]:8081/test/math.asmx/Add" method="POST">
```

当您通过浏览器直接访问一个 .asmx 文件时，通常会返回一个 HTML 页面，这个页面允许您以表单的形式输入参数并提交给 Web 服务。
这个表单页面是由服务器动态生成的，并不是预先写好的 HTML 文件。它通常是 ASP.NET 自动创建的，用于方便测试和调试 Web 服务的方法。

解决方法：  
使用本机IP地址访问，例如 `http://172.22.48.26:8081/test/math.asmx` 。
我已经在窗口初始化时把本机IP地址添加到IP列表中了。

```csharp
        private void Form1_Load(object sender, EventArgs e)
        {
            // 获取本机所有 IP 地址
            var addresses = Dns.GetHostAddresses("");
            foreach (var address in addresses)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine(address);
                    lbPrefix.Items.Add($"http://{address}:8081/");
                }
            }
        }
```

### 2024-08-01 解决【拒绝访问】

```
System.Net.HttpListenerException (0x80004005): 拒绝访问。
   在 System.Net.HttpListener.AddAllPrefixes()
   在 System.Net.HttpListener.Start()
   在 HttpListenerLibrary.HttpListenerWrapper.Start() 位置 D:\DevWork\Learn\03_Develop\simple-iis-asmx\SampleIIS\HttpListenerLibrary\HttpListenerWrapper.cs:行号 56
   在 System.Runtime.Remoting.Messaging.StackBuilderSink._PrivateProcessMessage(IntPtr md, Object[] args, Object server, Object[]& outArgs)
   在 System.Runtime.Remoting.Messaging.StackBuilderSink.SyncProcessMessage(IMessage msg)
```

- 项目增加app.manifest文件（也有采用）
- 手动启动exe以管理员方式运行
- rider以管理员方式运行（我的方案：这样可以调试运行）

### 2024-08-02 重整项目结构

- 将所有代码移动到 Src 目录下
- 将 html 和 asmx 移动到 WebRoot 项目下。
- 主项目输出到 Output\bin
    - AsmxServer.Wpf
- 库项目输出到 Output\bin
    - HttpListenerLibrary 基础库：运行
    - HisServiceLibrary 业务库：服务


### 2024-08-02 SendRedirect
处理到 / 时，告知客户端重定向到 index.html

如果代码实现自己的托管环境，则需要扩展此类以调用 ProcessRequest 方法。
代码还可以创建派生类，以便在给定应用程序中执行子请求，以便将其实例传递给 ProcessRequest。

System.Web.HttpWorkerRequest.HeaderLocation

